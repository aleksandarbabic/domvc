package prototype;

import java.awt.Color;

import rekapitulacijaDO.Point;

public class TestPrototype {

	public static void main(String[] args) {
		
		//Shallow clone
	/*	GreatBear gb = new GreatBear();
		gb.setConstellation("Veliki medved");
		gb.setAlioth(new Point(10,10, Color.BLUE));
		
		GreatBear gbShallow = gb.clone();
		System.out.println("Original :" + gb.getConstellation());
		System.out.println("Original :" + gb.getAlioth());
		
		System.out.println("Shallow :" + gbShallow.getConstellation());
		System.out.println("Shallow :" + gbShallow.getAlioth());
		*/
		//Deep clone
		GreatBearDeep gbD = new GreatBearDeep();
		
		gbD.setConstellation("Veliki medved");
		gbD.setAlioth(new Point(10,10, Color.BLUE)); 
		
		GreatBearDeep gbDeep = gbD.clone();
		System.out.println("Original :" + gbD.getConstellation());
		System.out.println("Original :" + gbD.getAlioth());
		
		System.out.println("Deep :" + gbDeep.getConstellation());
		System.out.println("Deep :" + gbDeep.getAlioth());
	}

}
