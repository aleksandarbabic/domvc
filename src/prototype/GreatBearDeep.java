package prototype;

import rekapitulacijaDO.Point;

public class GreatBearDeep implements Cloneable {
	private String constellation;
	private Point idubhe; //star in Great bear
	private Point merak; //star in Great bear
	private Point phad; //star in Great bear
	private Point megrez; //star in Great bear
	private Point alioth; //star in Great bear
	private Point mizar; //star in Great bear
	private Point alkaid; //star in Great bear
	
	public String getConstellation() {
		return constellation;
	}
	public void setConstellation(String constellation) {
		this.constellation = constellation;
	}
	public Point getIdubhe() {
		return idubhe;
	}
	public void setIdubhe(Point idubhe) {
		this.idubhe = idubhe;
	}
	public Point getMerak() {
		return merak;
	}
	public void setMerak(Point merak) {
		this.merak = merak;
	}
	public Point getPhad() {
		return phad;
	}
	public void setPhad(Point phad) {
		this.phad = phad;
	}
	public Point getMegrez() {
		return megrez;
	}
	public void setMegrez(Point megrez) {
		this.megrez = megrez;
	}
	public Point getAlioth() {
		return alioth;
	}
	public void setAlioth(Point alioth) {
		this.alioth = alioth;
	}
	public Point getMizar() {
		return mizar;
	}
	public void setMizar(Point mizar) {
		this.mizar = mizar;
	}
	public Point getAlkaid() {
		return alkaid;
	}
	public void setAlkaid(Point alkaid) {
		this.alkaid = alkaid;
	}
	public GreatBearDeep clone() {
		GreatBearDeep clone = new GreatBearDeep();
		clone.setConstellation(constellation);
		clone.setIdubhe(getIdubhe());
		clone.setMerak(merak);
		clone.setMegrez(megrez);
		clone.setAlioth(alioth);
		clone.setPhad(phad);
		clone.setMizar(mizar);
		return clone;
	}
	
}
