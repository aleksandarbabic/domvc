package observer2;

public class TestObserver {
	public static void main(String[] args) {
		StringObserver stringObserver = new StringObserver();
		ObservableString observableString = new ObservableString();
		observableString.addObserver(stringObserver);
		
	
		stringObserver.setOs(observableString);
		observableString.setTekst("prva promena");
		observableString.setTekst("druga promena");
	}
}
