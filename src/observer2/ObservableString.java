package observer2;

import java.util.Observable;

public class ObservableString extends Observable {
	private String tekst;
	
	public String getTekst() {
		return tekst;
	}
	
	public void setTekst(String tekst) {
		this.tekst = tekst;
		setChanged();
		notifyObservers();
	}
}
