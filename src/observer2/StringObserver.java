package observer2;

import java.util.Observable;
import java.util.Observer;

public class StringObserver implements Observer {
	
	private ObservableString os;
	
	public void setOs(ObservableString os) {
		this.os = os;
	}
	
	@Override
	public void update(Observable arg0, Object arg1) {
		if(arg0 == os)
			System.out.println(((ObservableString) arg0).getTekst());
	}

}
