package state;

public interface CarState {
	void turnOff();
	void turnOn();
	void idle();
	void move();
}
