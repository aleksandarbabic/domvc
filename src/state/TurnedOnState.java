package state;

public class TurnedOnState implements CarState {
	private Car car;
	
	public TurnedOnState(Car car) {
		this.car = car;
	}
	
	@Override
	public void turnOff() {
		System.out.println("You can only turn off idle car");
	}

	@Override
	public void turnOn() {
		System.out.println("Car is already turned on");
	}

	@Override
	public void idle() {
		System.out.println("Car is idle");
		car.setCurrentState(car.getIdleState());
	}

	@Override
	public void move() {
		System.out.println("Car is moving");
		car.setCurrentState(car.getMovingState());
	}

}
