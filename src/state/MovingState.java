package state;

public class MovingState implements CarState {
	private Car car;

	public MovingState(Car car) {
		this.car = car;
	}
	@Override
	public void turnOff() {
		System.out.println("Car cannot be turned off when is moving");
	}

	@Override
	public void turnOn() {
		System.out.println("Car is already turned on");
	}

	@Override
	public void idle() {
		System.out.println("Car is now idle");
		car.setCurrentState(car.getIdleState());
	}

	@Override
	public void move() {
		System.out.println("Car is already moving");
	}

}
