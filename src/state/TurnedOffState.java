package state;

public class TurnedOffState implements CarState {
	private Car car;
	
	public TurnedOffState(Car car) {
		this.car = car;
	}
	
	@Override
	public void turnOff() {
		System.out.println("Car is already turned off");
	}

	@Override
	public void turnOn() {
		System.out.println("Car is turned on");
		car.setCurrentState(car.getTurnedOnState());
	}

	@Override
	public void idle() {
		System.out.println("Car cannot be idle when turned off");
	}

	@Override
	public void move() {
		System.out.println("Car cannot move when turned off");
	}

}
