package state;

public class Car {
	private CarState turnedOffState;
	private CarState turnedOnState;
	private CarState idleState;
	private CarState movingState;
	
	private CarState currentState;
	
	public Car() {
		turnedOffState = new TurnedOffState(this);
		turnedOnState = new TurnedOnState(this);
		idleState = new IdleState(this);
		movingState = new MovingState(this);
		
		currentState = turnedOffState;
	}
	
	public void turnOn() {
		currentState.turnOn();
	}
	
	public void turnOff() {
		currentState.turnOff();
	}
	
	public void idle() {
		currentState.idle();
	}
	
	public void move() {
		currentState.move();
	}

	public CarState getTurnedOffState() {
		return turnedOffState;
	}

	public CarState getTurnedOnState() {
		return turnedOnState;
	}

	public CarState getIdleState() {
		return idleState;
	}

	public CarState getMovingState() {
		return movingState;
	}

	public void setCurrentState(CarState cs) {
		this.currentState = cs;
	}
}
