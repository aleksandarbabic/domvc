package state;

public class IdleState implements CarState {
	private Car car;

	public IdleState(Car car) {
		this.car = car;
	}

	@Override
	public void turnOff() {
		System.out.println("Car is now turned off");
		car.setCurrentState(car.getTurnedOffState());
	}

	@Override
	public void turnOn() {
		System.out.println("Car is already turned on");
	}

	@Override
	public void idle() {
		System.out.println("Car is already idle");
	}

	@Override
	public void move() {
		System.out.println("Car is moving");
		car.setCurrentState(car.getMovingState());
	}

}
