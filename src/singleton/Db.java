package singleton;

public class Db {
	private static Db instance = new Db();
	private static Db instanceLazy;
	
	private Db() {}
	
	public static Db getInstance() { return instance; }
	
	public static Db getInstanceLazy() {
		if(instanceLazy == null) {
			synchronized(Db.class) {
				if(instanceLazy == null) {
					instanceLazy = new Db();
				}
			}
		}
		return instanceLazy; 
	}
	
	public void connect() {
		System.out.println("Connected!");
	}
	
	public void disonnect() {
		System.out.println("Disconnected!");
	}
}
