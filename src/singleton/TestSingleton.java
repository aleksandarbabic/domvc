package singleton;

public class TestSingleton {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Db db = Db.getInstance();
		Db db1 = Db.getInstance();
		System.out.println(db + " " + db1);
		
		System.out.println("Lazy :");
		Db dbLazy = Db.getInstanceLazy();
		Db dbLazy1 = Db.getInstanceLazy();
		System.out.println(dbLazy + " " + dbLazy1);
	}

}
