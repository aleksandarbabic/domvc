package command;

import java.awt.Color;

import mvc.DrawingModel;
import rekapitulacijaDO.Point;

public class TestCommand {

	public static void main(String[] args) {
		DrawingModel model = new DrawingModel();
		
		Point t1 = new Point(10,10, Color.BLACK);
		Point t2 = new Point(20,20, Color.BLACK);
		
		
		//Add point CMD
		CmdAddPoint c1 = new CmdAddPoint(model, t1);
		c1.execute();
		System.out.println("Adding point..");
		System.out.println("Size of list: " + model.getShapes().size());
		System.out.println("Removing point..");
		c1.unexecute();
		System.out.println("Size of list: " + model.getShapes().size());
		c1.execute();
		System.out.println("Adding point..");
		
		//Remove point CMD
		CmdRemovePoint c2 = new CmdRemovePoint(model, t2);
		c2.execute();
		System.out.println("Removing point 2..");
		System.out.println("Size of list: " + model.getShapes().size());
		c2.unexecute();
		System.out.println("Readding point 2..");
		System.out.println("Size of list: " + model.getShapes().size());
		
		
	}
}
