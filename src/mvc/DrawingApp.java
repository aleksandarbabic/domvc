package mvc;

public class DrawingApp {

	public static void main(String[] args) {
		DrawingModel model = new DrawingModel();
		DrawingFrame frame = new DrawingFrame();
		DrawingController controller = new DrawingController(model,frame);
		//frame.setController(controller);
		frame.addMouseClickedObserver(controller);
		frame.getView().setModel(model);
	}

}
