package mvc;

import java.awt.Graphics;
import java.util.Iterator;

import javax.swing.JPanel;

import rekapitulacijaDO.Shape;

public class DrawingView extends JPanel {
	DrawingModel model;

	public void setModel(DrawingModel model) {
		this.model = model;
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		if (model != null) {
			Iterator<Shape> it = model.getShapes().iterator();
			while (it.hasNext()) {
				it.next().drawShape(g);
			}
		}
		repaint();
	}

}
