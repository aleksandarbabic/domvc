package mvc;

import java.awt.event.MouseEvent;

public interface MouseClickedObserver {
	void mouseClicked(MouseEvent e);
}
