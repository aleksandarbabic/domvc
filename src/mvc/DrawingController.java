package mvc;

import java.awt.Color;
import java.awt.event.MouseEvent;

import composite.SremBanatBacka;
import rekapitulacijaDO.Point;

public class DrawingController implements MouseClickedObserver {
	private DrawingFrame frame;
	private DrawingModel model;

	public DrawingController(DrawingModel model, DrawingFrame frame) {
		this.model = model;
		this.frame = frame;
	}

	/*public void viewMouseClicked(MouseEvent e) {
		
		//Force composite pattern test
		Point srem = new Point(20,20, Color.BLUE);
		Point banat = new Point(30,20, Color.BLUE);
		Point backa = new Point(25,12, Color.BLUE);
		
		SremBanatBacka sbb = new SremBanatBacka();
		sbb.add(srem);
		sbb.add(banat);
		sbb.add(backa);
		
		model.add(sbb);
		model.getShapes().add(new Point(e.getX(),e.getY(), Color.BLACK));
		System.out.println(model.getShapes().size());
		
		
		//Standard method for handling mouse click
		model.getShapes().add(new Point(e.getX(),e.getY(), Color.BLACK));
		System.out.println(model.getShapes().size());
	}*/

	@Override
	public void mouseClicked(MouseEvent e) {
		model.getShapes().add(new Point(e.getX(),e.getY(), Color.BLACK));
		System.out.println(model.getShapes().size());
	}

}
