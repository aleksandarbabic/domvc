package mvc;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class DrawingFrame extends JFrame {
	private MouseClickedObserver mouseClickedObserver;
	private DrawingController controller;
	private DrawingView view = new DrawingView();

	public DrawingFrame() {
		//INIT
		this.setSize(600,400);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	
		view.setBackground(Color.WHITE);
		getContentPane().add(view, BorderLayout.CENTER);
		view.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				mouseClickedObserver.mouseClicked(e);
			}
		});
	}

	public void setController(DrawingController controller) {
		this.controller = controller;
	}
	
	public void addMouseClickedObserver(MouseClickedObserver mouseClickedObserver) {
		this.mouseClickedObserver = mouseClickedObserver;
	}

	public DrawingView getView() {
		return view;
	}

	
}
