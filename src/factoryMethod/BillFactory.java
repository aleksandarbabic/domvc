package factoryMethod;

public abstract class BillFactory {
	public static final int ELECTRICITY = 0, UTILITY = 1, GAS = 2;
	
	public static Bill createBill(int i) {
		switch (i) {
		case ELECTRICITY: 
			return new ElectricityBill();
		case UTILITY: 
			return new UtilityBill();
		case GAS: 
			return new GasBill();
		default: return null;
		}
	}
}
