package factoryMethod;

public class UtilityBill extends Bill {

	@Override
	public void calculateTotal() {
		System.out.println("Utility bill is too high!");
	}
	
}
