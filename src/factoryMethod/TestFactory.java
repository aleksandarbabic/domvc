package factoryMethod;

public class TestFactory {
	public static void main(String[] args) {
		Bill bill = BillFactory.createBill(BillFactory.UTILITY);
		bill.calculateTotal();
	}
}
