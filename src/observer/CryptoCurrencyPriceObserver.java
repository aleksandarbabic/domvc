package observer;

public class CryptoCurrencyPriceObserver implements Observer {
	private double bitcoinPrice;
	private double ethereumPrice;
	
	@Override
	public void update(double bitcoinPrice, double ethereumPrice) {
		this.bitcoinPrice = bitcoinPrice;
		this.ethereumPrice = ethereumPrice;
		printToConsole();
	}
	
	public void printToConsole() {
		System.out.println(String.format("Cena bitcoina je %s , cena ethereuma je %s", bitcoinPrice, ethereumPrice));
	}

}
