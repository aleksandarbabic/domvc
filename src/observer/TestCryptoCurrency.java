package observer;

public class TestCryptoCurrency {

	public static void main(String[] args) {
		CryptoCurrencyPriceObserver cryptoCurObserver = new CryptoCurrencyPriceObserver();
		CryptoCurrencyPrice cryptoCurPrice = new CryptoCurrencyPrice(); 
		cryptoCurPrice.addObserver(cryptoCurObserver);
		cryptoCurPrice.setBitcoinPrice(8500);
		cryptoCurPrice.setEthereumPrice(6500);
		cryptoCurPrice.setBitcoinPrice(9500);
		cryptoCurPrice.setEthereumPrice(10500);
	}

}
