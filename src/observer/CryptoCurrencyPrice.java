package observer;

import java.util.ArrayList;

public class CryptoCurrencyPrice implements Subject{
	private double bitcoinPrice;
	private double ethereumPrice;
	private ArrayList<Observer> observers = new ArrayList<Observer>();
	@Override
	public void addObserver(Observer observer) {
		observers.add(observer);
	}

	@Override
	public void removeObserver(Observer observer) {
		observers.remove(observer);
	}

	@Override
	public void notifyAllObservers() {
		for(Observer ob : observers) {
			ob.update(bitcoinPrice, ethereumPrice);
		}
	}
	
	public void setBitcoinPrice(double bitcoinPrice) {
		this.bitcoinPrice = bitcoinPrice;
		notifyAllObservers();
	}

	public void setEthereumPrice(double ethereumPrice) {
		this.ethereumPrice = ethereumPrice;
		notifyAllObservers();
	}

}
