package strategy;

public class TestStrategy {

	public static void main(String[] args) {
		PaymentManager pm = new PaymentManager(new CryptoCurrencyPayment());
		pm.pay(250);
		
		PaymentManager pm2 = new PaymentManager(new CreditCardPayment());
		pm.pay(2500);
	}

}
