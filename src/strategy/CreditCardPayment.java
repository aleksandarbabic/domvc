package strategy;

public class CreditCardPayment implements Payment {
	private String name;
	private String creditCardNumber;
	private String cvc;
	private String expireDate;
	
	@Override
	public void pay(int amount) {
		System.out.println("paid " + amount + " by credit card");
	}

}
