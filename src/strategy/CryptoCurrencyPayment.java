package strategy;

public class CryptoCurrencyPayment implements Payment {
	private String eWalletAddress;
	
	@Override
	public void pay(int amount) {
		System.out.println("paid " + amount + " by cryptocurrency");
	}

}
