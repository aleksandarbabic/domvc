package facade;

public class AccountCheck {
	private int accountNumber;

	public AccountCheck(int accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public boolean isActive() {
		return accountNumber == 123456;
	}

}
