package facade;

public class ATM {
	private int accountNumber;
	private int pin;
	
	private AccountCheck accountCheck;
	private PinCheck pinCheck;
	private FundsCheck fundsCheck;
	
	public ATM(int accountNumber, int pin) {
		this.accountNumber = accountNumber;
		this.pin = pin;
		
		accountCheck = new AccountCheck(this.accountNumber);
		pinCheck = new PinCheck(this.pin);
		fundsCheck = new FundsCheck(this.accountNumber);
	}
	
	public void withdrawFunds(int amount) {
		if(accountCheck.isActive() && 
				pinCheck.isPinValid() && 
				fundsCheck.hasFunds(amount)) {
			fundsCheck.withdraw(amount);
		}
	}
	
	public void depositFunds(int amount) {
		if(accountCheck.isActive() && pinCheck.isPinValid()) {
			fundsCheck.deposit(amount);
		}
	}
}
