package facade;

public class PinCheck {
	private int pin;

	public PinCheck(int pin) {
		this.pin = pin;
	}
	
	public boolean isPinValid() {
		return pin==1111;
	}
	
}
