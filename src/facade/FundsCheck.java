package facade;

public class FundsCheck {
	private int accountNumber;
	private int funds = 1000;
	private boolean enoughFunds;
	
	public FundsCheck(int accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public boolean hasFunds(int amount) {
		enoughFunds = funds >= amount;
		return enoughFunds;
	}
	
	public void withdraw(int amount) {
		funds -= amount;
		System.out.println("Withdraw is complete, take your cash");
		System.out.println("Available funds - " + funds);
	}
	
	public void deposit(int amount) {
		funds += amount;
		System.out.println("Deposit is complete");
		System.out.println("Available funds - " + funds);
	}
	
}
