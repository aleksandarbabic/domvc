package facade;

public class TestFacade {

	public static void main(String[] args) {
		ATM atm = new ATM(123456, 1111);
		atm.depositFunds(1000);
		atm.withdrawFunds(500);
		atm.depositFunds(2000);
		atm.withdrawFunds(3500);
		atm.withdrawFunds(1500);
	}

}
