package builder;

public class Sandwich {
	private String bread;
	private String spread;
	private String meat;
	private String salad;
	private String dressing;
	
	public Sandwich(Builder builder) {
		this.bread = builder.bread;
		this.meat = builder.meat;
		this.spread = builder.spread;
		this.salad = builder.salad;
		this.dressing = builder.dressing;
	}
	
	public String getBread() {
		return bread;
	}
	public String getMeat() {
		return meat;
	}
	public String getSalad() {
		return salad;
	}
	public String getSpread() {
		return spread;
	}
	public String getDressing() {
		return dressing;
	}
	
	public static class Builder {
		private final String bread;
		private final String spread;
		private String meat;
		private String salad;
		private String dressing;
		
		public Builder(String bread, String spread) {
			this.bread = bread;
			this.spread = spread;
		}
		
		public Builder meat(String meat) {
			this.meat = meat;
			return this;
		}
		
		public Builder salad(String salad) {
			this.salad = salad;
			return this;
		}
		
		public Builder dressing(String dressing) {
			this.dressing = dressing;
			return this;
		}
		
		public Sandwich build() {
			return new Sandwich(this);
		}
	}
	
}
