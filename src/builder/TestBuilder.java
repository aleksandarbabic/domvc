package builder;

public class TestBuilder {

	public static void main(String[] args) {
		Sandwich.Builder builder = new Sandwich.Builder("beli hleb", "tzatziki");
		Sandwich index = builder
			.dressing("senf")
			.meat("sunka")
			.salad("kupus")
			.build();
		
		System.out.println(index.getBread() + index.getDressing() + index.getMeat() + index.getSalad() + index.getSpread());
	}

}
