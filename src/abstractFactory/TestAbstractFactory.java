package abstractFactory;

public class TestAbstractFactory {

	public static void main(String[] args) {
		AnimalFactory afW = AnimalFactoryFactory.createAnimalFactoryFactory("wild");
		AnimalFactory afD = AnimalFactoryFactory.createAnimalFactoryFactory("domestic");
		
		Animal a1 = afD.createAnimal("cow");
		Animal a2 = afW.createAnimal("bear");
		
		System.out.println(a1.goes());
		System.out.println(a2.goes());
	}

}
