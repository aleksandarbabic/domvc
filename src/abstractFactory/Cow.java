package abstractFactory;

public class Cow extends Animal {

	@Override
	public String goes() {
		return "Moo";
	}
	
}
