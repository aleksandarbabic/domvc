package abstractFactory;

public abstract class Animal {
	public abstract String goes();
}
