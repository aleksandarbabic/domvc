package abstractFactory;

public class Dinosaur extends Animal {

	@Override
	public String goes() {
		return "GRRRRRRR";
	}

}
