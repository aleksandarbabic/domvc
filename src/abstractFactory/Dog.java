package abstractFactory;

public class Dog extends Animal {

	@Override
	public String goes() {
		return "Bark";
	}
	
}
