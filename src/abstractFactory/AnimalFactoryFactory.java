package abstractFactory;

public class AnimalFactoryFactory {
	public static AnimalFactory createAnimalFactoryFactory(String s) {
		switch(s) {
		case "domestic": return new DomesticAnimalFactory();
		case "wild": return new WildAnimalFactory();
		default: return null;
		}
	}
}
