package abstractFactory;

public class DomesticAnimalFactory extends AnimalFactory {

	@Override
	public Animal createAnimal(String s) {
		switch(s) {
			case "dog": return new Dog();
			case "cow": return new Cow();
			default: return null;
		}
	}

}
